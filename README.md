# hissyblog-freebsd-verify-java

Simple Java toolchain verification by way of hello world.

This forms part of a series of blog posts on [Hissyblog](https://hissyfit.gitlab.io/hissyblog/) regarding the use of FreeBSD for software development.

## Prerequisites

An installation of [OpenJDK 20](https://openjdk.org).

## Building and Running

```shell
./gradlew run
```

## Copyright and License

Copyright © 2023 Kevin Poalses

Distributed under the [Simplified BSD License](./LICENSE.md).
