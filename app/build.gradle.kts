plugins {
    application
}

repositories {
    mavenCentral()
}

dependencies {
}

// Apply a specific Java toolchain to ease working on different environments.
java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(20))
    }
}

application {
    mainClass.set("io.gitlab.hissyfit.hissyblog.freebsd.verify.App")
}
